from django.urls import path
from .views import MovieDetailView, MovieView, ReviewView, UserView, login

urlpatterns = [
    path('accounts/', UserView.as_view()),
    path('login/', login),
    path('movies/', MovieView.as_view()),
    path('movies/<int:pk>/', MovieDetailView.as_view()),
    path('movies/<int:movie_id>/review/', ReviewView.as_view())
]