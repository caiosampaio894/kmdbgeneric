from rest_framework import serializers
from .models import Genre, Movie, Review
from django.contrib.auth.models import User
import ipdb

class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'first_name', 'last_name', 'is_superuser', 'is_staff']
        extra_kwargs =  {'password': {'write_only': True }}
    def create(self, validated_data):
        return User.objects.create_user(**validated_data)
        

class UserReviewSerializers(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    first_name = serializers.CharField()
    last_name = serializers.CharField()



class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ['id', 'name']


class MovieSerializer(serializers.ModelSerializer):
    genres = GenreSerializer(many=True)

    class Meta:
        model = Movie
        fields = ['id', 'title', 'duration', 'genres', 'premiere', 'classification', 'synopsis']

    def create(self, validated_data):
        genres_data = validated_data.pop('genres')
        movie = Movie.objects.create(**validated_data)

        for genre_data in genres_data:
            genre = Genre.objects.create(**genre_data)
            movie.genres.add(genre)
        
        return movie

    def update(self, instance, validated_data):
        genres_data = validated_data.pop('genres')
        movie = Movie.objects.filter(id=instance.id).update(**validated_data)

        for genre_data in genres_data:
            genre = Genre.objects.create(**genre_data)
            instance.genres.add(genre)
        
        instance.refresh_from_db()
        
        return instance


class ReviewSerializers(serializers.ModelSerializer):
    critic = UserReviewSerializers(read_only=True)
    
    class Meta:
        model = Review
        fields = ['id', 'critic', 'stars', 'review', 'spoilers']

    def create(self, validated_data):
        ipdb.set_trace()
        critic_data = validated_data.pop('critic')
        review = Review.objects.create(**validated_data)

        for critic in critic_data:
            critics = User.object.create(**critic)
            review.critic.add(critics)
        
        return review
   




