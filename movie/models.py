from django.db import models
from django.contrib.auth.models import User

class Review(models.Model):
    stars = models.IntegerField()
    review = models.CharField(max_length=255)
    spoilers = models.BooleanField()
    critic = models.ForeignKey(User, on_delete=models.CASCADE)

class Movie(models.Model):
    title = models.CharField(max_length=255)
    duration = models.CharField(max_length=255)
    genres = models.ManyToManyField('Genre')
    premiere = models.DateField(max_length=10)
    classification = models.IntegerField()
    synopsis = models.CharField(max_length=255)


class Genre(models.Model):
    name = models.CharField(max_length=255)
