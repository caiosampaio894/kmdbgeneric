from django.shortcuts import render
from rest_framework import serializers
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, RetrieveUpdateDestroyAPIView, CreateAPIView
from .serializers import MovieSerializer, ReviewSerializers, UserSerializers
from movie.models import Movie, Review
from django.contrib.auth.models import User
from rest_framework.decorators import api_view, authentication_classes
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.http.response import HttpResponseBadRequest
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated



class UserView(CreateAPIView):
    permission_classes = [IsAuthenticated]

    queryset = User.objects.all()
    serializer_class = UserSerializers

    def get_authenticators(self):
        if self.request.method != 'GET':
            authentication_classes = [TokenAuthentication]
            return  [authenticator() for authenticator in authentication_classes]

#base permissions
# dentro do permissions.py verificar se é metohod safe(get) retorna True // se o usuário é super_user e autenticado.

@api_view(['POST'])
def login(request):
    try:
        username = request.data['username']
        password = request.data['password']
    

        user = authenticate(username=username, password=password)
        
        if user:
            token = Token.objects.get_or_create(user=user)[0]
            return Response({'token': token.key})

        return Response({'msg': 'Invalid Credentials'})

    except KeyError:
        return HttpResponseBadRequest()


class MovieView(ListCreateAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer

class MovieDetailView(RetrieveUpdateDestroyAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer



class ReviewView(ListCreateAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializers